import React from 'react';
import styles from '../../src/Style.module.css';
import {  useDrop } from "react-dnd";


//https://react-dnd.github.io/react-dnd/docs/api/use-drop
const Field =() => { 

    const [, drop] = useDrop({ //Specification Object Members:
        accept: 'laiva', //Required. Type laiva accepted
        drop: () => ({name: 'solu', id: "" }), //palauttaa tämän Laiva.js monitor.getDropResult() -> korvaa id viittauksella soluun?
        collect: (monitor) => ({ //Collect: Optional. The collecting function. It should return a plain object of the props to return for injection into your component.
          
          
            //props to return for injection into your component
        }),
       
    });
   
    /* create a DropTarget and define a drop() function within its source,
     and whatever that returns will be what is returned by the monitor.getDropResult() */

    console.log('options');

return ( 
//wrap our components that are taking part in drag and drop with DndProvider.
// <DndProvider backend={HTML5Backend}></DndProvider>
// done in layout

<div>
    
<table className={styles.table}>
    <tr className={styles.tr}> {/*rivi 1*/}  {/*https://reactjs.org/link/strict-mode-string-ref */}
        <td className={styles.td} id = {1.1} ref = {drop} > <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td> {/*solu*/} {/*drop -> lisää solu id? */}
        <td className={styles.td} id = {1.2} > <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.3} > <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.4}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.5}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.6}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.7}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.8}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.9}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
        <td className={styles.td} id = {1.10}> <img src="https://i.imgur.com/e61GHQl.png" alt= "."></img>  </td>
    </tr>

    <tr className={styles.tr}> {/*rivi 2*/}  
        <td className={styles.td} id = {2.1}></td> {/*solu*/}
        <td className={styles.td} id = {2.2}></td>
        <td className={styles.td} id = {2.3}></td>
        <td className={styles.td} id = {2.4}></td>
        <td className={styles.td} id = {2.5}></td>
        <td className={styles.td} id = {2.6}></td>
        <td className={styles.td} id = {2.7}></td>
        <td className={styles.td} id = {2.8}></td>
        <td className={styles.td} id = {2.9}></td>
        <td className={styles.td} id = {2.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 3*/}  
        <td className={styles.td} id = {3.1}></td> {/*solu*/}
        <td className={styles.td} id = {3.2}></td>
        <td className={styles.td} id = {3.3}></td>
        <td className={styles.td} id = {3.4}></td>
        <td className={styles.td} id = {3.5}></td>
        <td className={styles.td} id = {3.6}></td>
        <td className={styles.td} id = {3.7}></td>
        <td className={styles.td} id = {3.8}></td>
        <td className={styles.td} id = {3.9}></td>
        <td className={styles.td} id = {3.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 4*/}  
        <td className={styles.td} id = {4.1}></td> {/*solu*/}
        <td className={styles.td} id = {4.2}></td>
        <td className={styles.td} id = {4.3}></td>
        <td className={styles.td} id = {4.4}></td>
        <td className={styles.td} id = {4.5}></td>
        <td className={styles.td} id = {4.6}></td>
        <td className={styles.td} id = {4.7}></td>
        <td className={styles.td} id = {4.8}></td>
        <td className={styles.td} id = {4.9}></td>
        <td className={styles.td} id = {4.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 5*/}  
        <td className={styles.td} id = {5.1}></td> {/*solu*/}
        <td className={styles.td} id = {5.2}></td>
        <td className={styles.td} id = {5.3}></td>
        <td className={styles.td} id = {5.4}></td>
        <td className={styles.td} id = {5.5}></td>
        <td className={styles.td} id = {5.6}></td>
        <td className={styles.td} id = {5.7}></td>
        <td className={styles.td} id = {5.8}></td>
        <td className={styles.td} id = {5.9}></td>
        <td className={styles.td} id = {5.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 6*/}  
        <td className={styles.td} id = {6.1}></td> {/*solu*/}
        <td className={styles.td} id = {6.2}></td>
        <td className={styles.td} id = {6.3}></td>
        <td className={styles.td} id = {6.4}></td>
        <td className={styles.td} id = {6.5}></td>
        <td className={styles.td} id = {6.6}></td>
        <td className={styles.td} id = {6.7}></td>
        <td className={styles.td} id = {6.8}></td>
        <td className={styles.td} id = {6.9}></td>
        <td className={styles.td} id = {6.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 7*/}  
        <td className={styles.td} id = {7.1}></td> {/*solu*/}
        <td className={styles.td} id = {7.2}></td>
        <td className={styles.td} id = {7.3}></td>
        <td className={styles.td} id = {7.4}></td>
        <td className={styles.td} id = {7.5}></td>
        <td className={styles.td} id = {7.6}></td>
        <td className={styles.td} id = {7.7}></td>
        <td className={styles.td} id = {7.8}></td>
        <td className={styles.td} id = {7.9}></td>
        <td className={styles.td} id = {7.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 8*/}  
        <td className={styles.td} id = {8.1}></td> {/*solu*/}
        <td className={styles.td} id = {8.2}></td>
        <td className={styles.td} id = {8.3}></td>
        <td className={styles.td} id = {8.4}></td>
        <td className={styles.td} id = {8.5}></td>
        <td className={styles.td} id = {8.6}></td>
        <td className={styles.td} id = {8.7}></td>
        <td className={styles.td} id = {8.8}></td>
        <td className={styles.td} id = {8.9}></td>
        <td className={styles.td} id = {8.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 9*/}  
        <td className={styles.td} id = {9.1}></td> {/*solu*/}
        <td className={styles.td} id = {9.2}></td>
        <td className={styles.td} id = {9.3}></td>
        <td className={styles.td} id = {9.4}></td>
        <td className={styles.td} id = {9.5}></td>
        <td className={styles.td} id = {9.6}></td>
        <td className={styles.td} id = {9.7}></td>
        <td className={styles.td} id = {9.8}></td>
        <td className={styles.td} id = {9.9}></td>
        <td className={styles.td} id = {9.10}></td>
    </tr>

    <tr className={styles.tr}> {/*rivi 10*/}  
        <td className={styles.td} id = {10.1}></td> {/*solu*/}
        <td className={styles.td} id = {10.2}></td>
        <td className={styles.td} id = {10.3}></td>
        <td className={styles.td} id = {10.4}></td>
        <td className={styles.td} id = {10.5}></td>
        <td className={styles.td} id = {10.6}></td>
        <td className={styles.td} id = {10.7}></td>
        <td className={styles.td} id = {10.8}></td>
        <td className={styles.td} id = {10.9}></td>
        <td className={styles.td} id = {10.10}></td>
    </tr>

 

</table>

</div>

)};


export default Field;