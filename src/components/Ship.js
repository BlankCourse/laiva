import React from 'react';


export default class Ship extends React.Component {
  constructor() {
    super();

    this.ships = [
      {name: 'lentotukialus', length: 5},
      {name: 'taistelulaiva', length: 4},
      {name: 'sukellusvene', length: 3},
      {name: 'silja line', length: 3},
      {name: 'Vene', length: 2},
    ];
    
  }


  render() {
    return (
      
      <div className = "container">
         
        <p>Valitse sijoitettava laiva</p>
       
        <div>
        {this.ships.map(ship => {
          
           return (
             
              <button key={ship.name} value={ship.name} >
                {`${ship.name} (${ship.length})`}
              </button>
            
          ) 
        })}
         </div>
         

      </div>
      
    )
  }
};