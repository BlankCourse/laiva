import styles from '../../src/Style.module.css';
import React from 'react';


class OpponentField extends React.Component {   
    render() {
        return (

/**OpponentField - tarkoitettu vihollisen kentäksi, mihin pelaaja klikkailee ja mihin tulee räjähdykset ja missed kuvakkeet. */
<table className={styles.table}>
    <tr className={styles.tr}> {/*rivi 1*/}  
        <td className={styles.td} id = {1.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {1.2}>2</td>
        <td className={styles.td} id = {1.3}>3</td>
        <td className={styles.td} id = {1.4}> 4</td>
        <td className={styles.td} id = {1.5}>5</td>
        <td className={styles.td} id = {1.6}>6</td>
        <td className={styles.td} id = {1.7}>7</td>
        <td className={styles.td} id = {1.8}>8</td>
        <td className={styles.td} id = {1.9}>9</td>
        <td className={styles.td} id = {1.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 2*/}  
        <td className={styles.td} id = {2.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {2.2}>2</td>
        <td className={styles.td} id = {2.3}>3</td>
        <td className={styles.td} id = {2.4}> 4</td>
        <td className={styles.td} id = {2.5}>5</td>
        <td className={styles.td} id = {2.6}>6</td>
        <td className={styles.td} id = {2.7}>7</td>
        <td className={styles.td} id = {2.8}>8</td>
        <td className={styles.td} id = {2.9}>9</td>
        <td className={styles.td} id = {2.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 3*/}  
        <td className={styles.td} id = {3.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {3.2}>2</td>
        <td className={styles.td} id = {3.3}>3</td>
        <td className={styles.td} id = {3.4}> 4</td>
        <td className={styles.td} id = {3.5}>5</td>
        <td className={styles.td} id = {3.6}>6</td>
        <td className={styles.td} id = {3.7}>7</td>
        <td className={styles.td} id = {3.8}>8</td>
        <td className={styles.td} id = {3.9}>9</td>
        <td className={styles.td} id = {3.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 4*/}  
        <td className={styles.td} id = {4.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {4.2}>2</td>
        <td className={styles.td} id = {4.3}>3</td>
        <td className={styles.td} id = {4.4}> 4</td>
        <td className={styles.td} id = {4.5}>5</td>
        <td className={styles.td} id = {4.6}>6</td>
        <td className={styles.td} id = {4.7}>7</td>
        <td className={styles.td} id = {4.8}>8</td>
        <td className={styles.td} id = {4.9}>9</td>
        <td className={styles.td} id = {4.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 5*/}  
        <td className={styles.td} id = {5.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {5.2}>2</td>
        <td className={styles.td} id = {5.3}>3</td>
        <td className={styles.td} id = {5.4}> 4</td>
        <td className={styles.td} id = {5.5}>5</td>
        <td className={styles.td} id = {5.6}>6</td>
        <td className={styles.td} id = {5.7}>7</td>
        <td className={styles.td} id = {5.8}>8</td>
        <td className={styles.td} id = {5.9}>9</td>
        <td className={styles.td} id = {5.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 6*/}  
        <td className={styles.td} id = {6.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {6.2}>2</td>
        <td className={styles.td} id = {6.3}>3</td>
        <td className={styles.td} id = {6.4}> 4</td>
        <td className={styles.td} id = {6.5}>5</td>
        <td className={styles.td} id = {6.6}>6</td>
        <td className={styles.td} id = {6.7}>7</td>
        <td className={styles.td} id = {6.8}>8</td>
        <td className={styles.td} id = {6.9}>9</td>
        <td className={styles.td} id = {6.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 7*/}  
        <td className={styles.td} id = {7.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {7.2}>2</td>
        <td className={styles.td} id = {7.3}>3</td>
        <td className={styles.td} id = {7.4}> 4</td>
        <td className={styles.td} id = {7.5}>5</td>
        <td className={styles.td} id = {7.6}>6</td>
        <td className={styles.td} id = {7.7}>7</td>
        <td className={styles.td} id = {7.8}>8</td>
        <td className={styles.td} id = {7.9}>9</td>
        <td className={styles.td} id = {7.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 8*/}  
        <td className={styles.td} id = {8.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {8.2}>2</td>
        <td className={styles.td} id = {8.3}>3</td>
        <td className={styles.td} id = {8.4}> 4</td>
        <td className={styles.td} id = {8.5}>5</td>
        <td className={styles.td} id = {8.6}>6</td>
        <td className={styles.td} id = {8.7}>7</td>
        <td className={styles.td} id = {8.8}>8</td>
        <td className={styles.td} id = {8.9}>9</td>
        <td className={styles.td} id = {8.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 9*/}  
        <td className={styles.td} id = {9.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {9.2}>2</td>
        <td className={styles.td} id = {9.3}>3</td>
        <td className={styles.td} id = {9.4}> 4</td>
        <td className={styles.td} id = {9.5}>5</td>
        <td className={styles.td} id = {9.6}>6</td>
        <td className={styles.td} id = {9.7}>7</td>
        <td className={styles.td} id = {9.8}>8</td>
        <td className={styles.td} id = {9.9}>9</td>
        <td className={styles.td} id = {9.10}>10</td>
    </tr>

    <tr className={styles.tr}> {/*rivi 10*/}  
        <td className={styles.td} id = {10.1}>1</td> {/*solu*/}
        <td className={styles.td} id = {10.2}>2</td>
        <td className={styles.td} id = {10.3}>3</td>
        <td className={styles.td} id = {10.4}> 4</td>
        <td className={styles.td} id = {10.5}>5</td>
        <td className={styles.td} id = {10.6}>6</td>
        <td className={styles.td} id = {10.7}>7</td>
        <td className={styles.td} id = {10.8}>8</td>
        <td className={styles.td} id = {10.9}>9</td>
        <td className={styles.td} id = {10.10}>10</td>
    </tr>

 

</table>
   )
}
}

export default OpponentField;